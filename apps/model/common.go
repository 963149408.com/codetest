package model

import (
	"gorm.io/gorm"
)

type DevInfo struct {
	gorm.Model
	Uid  string `gorm:"column:uid;primary_key;comment:主键"`
	Ip   string
	Name string
}
