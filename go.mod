module codetest

go 1.16

require github.com/gogf/gf v1.16.6

require (
	gorm.io/driver/sqlite v1.3.1
	gorm.io/gorm v1.23.2
)
