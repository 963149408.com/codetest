package main

import (
	"fmt"

	_ "codetest/router"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/guid"
	_ "gorm.io/driver/sqlite"
	//"reflect"
	//xmlpath "gopkg.in/xmlpath.v1"
	//goquery "github.com/PuerkitoBio/goquery"
)

type RetContent struct {
	Content string `json:"content"`
	Code    int    `json:"code"`
}

func MiddlewareHandlerResponse(r *ghttp.Request) {
	fmt.Println("hook success")
	r.Response.Header().Set("xx", "jjj")
	r.Middleware.Next()
}

func initServer() {
	v := g.View()
	s := g.Server()
	s.SetServerRoot("/html")
	s.SetPort(8080)
	/*
		s.Group("/*", func(group *ghttp.RouterGroup) {
			fmt.Println("hook start")
			group.Middleware(MiddlewareHandlerResponse)
		})
	*/

	_ = v.AddPath("template")
	v.SetDelimiters("${", "}")

	s.BindHandler("/hello", func(r *ghttp.Request) {
		r.Response.Write("世界你好")
	})
	s.Run()
}

func main() {
	fmt.Println("server start")
	fmt.Println(guid.S())
	initServer()
}
