package router

import (
	"codetest/apps/api"
	"codetest/apps/model"

	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func initDb() *gorm.DB {
	// 获取默认配置的数据库对象(配置名称为"default")
	db, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
	if err != nil {
		panic("error open db")
	}

	db.AutoMigrate(&model.DevInfo{})
	/*
		db.Create(&DevInfo{Ip:"192.168.0.1",Name:"u0_a245"})
		var u = new(DevInfo)
		db.First(u)
		fmt.Printf("%#v\n", u)
	*/
	return db
}

func init() {
	db := initDb()
	s := g.Server()
	apis := api.NewController(db)

	// Index
	//s.BindController("/", new(index.Controller))

	// Chat
	//s.BindController("/chat", new(chat.Controller))
	//s.BindController("/sync", new(sync.Controller))

	s.BindObject("/api", apis)
	// Api
	s.Group("/api", func(g *ghttp.RouterGroup) {
		//cors
		g.Middleware(MiddlewareCORS)
	})

}
