package zIo

import (
    "fmt"
    "net/http"
    "io/ioutil"
)

type HttpResp struct{
    Url string
    Content string
} 

func Get(url string) HttpResp {
    httpResp:=HttpResp{Url:url}
    resp,ret:=http.Get(url)
    if ret!=nil{
    } else {
        defer resp.Body.Close()
        buf,err:=ioutil.ReadAll(resp.Body)
        if err==nil {
            httpResp.Content=string(buf)
        }
    }
    return httpResp
}

func (a *HttpResp) Print(){
    fmt.Println(a.Content)
}

func Print(a *HttpResp){
    fmt.Println(a.Url)
}