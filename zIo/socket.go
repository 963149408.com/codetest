package zIo
import (
	"net"
	"bufio"
	"fmt"
)

type SocketManager struct{
	Conn net.Conn
	Ok bool
}

func (obj *SocketManager)InitClient(addr string) bool {
	var err error
	obj.Conn,err=net.Dial("tcp",addr)
	if err!=nil {
		obj.Ok=false
		return false
	}
	obj.Ok=true
	return true
}

func (obj *SocketManager)Test(){
	var header string
	header="GET / HTTP/1.1\r\nHost: tv.cctv.com\r\n\r\n\r\n"
	obj.Conn.Write([]byte(header))
	reader:=bufio.NewReader(obj.Conn)
	ret,_,_:=reader.ReadLine()
	fmt.Println(string(ret))
}


func (obj *SocketManager)Dispose(){
	if obj.Ok {
	obj.Conn.Close()
	}
}
