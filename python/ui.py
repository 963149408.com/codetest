# encoding=utf-8
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
from PIL import Image, ImageTk
import time
from datetime import datetime as dt
import os
import earnMoney as earn

class Application(tk.Frame):

    def __init__(self,master=None):
        tk.Frame.__init__(self, master)
        # 显示窗口，并使用grid布局
        self.master.title='welcome to video-captioning system'
        self.grid()
        self.createWidgets(master)
        
    def createWidgets(self,master):
        master.rowconfigure(0,weight=1)
        master.columnconfigure(0,weight=1)
        self.grid(row=0,column=0,sticky=tk.NSEW)
        self.rowconfigure(0,weight=40)
        self.rowconfigure(1,weight=1)
        self.columnconfigure(0,weight=1)
        
        self.panewin=tk.Frame(self)#添加水平方向的推拉窗组件
        self.panewin.rowconfigure(0,weight=1)
        self.panewin.columnconfigure(0,weight=1)
        self.panewin.columnconfigure(1,weight=1)
        self.panewin.grid(row=0,column=0,sticky=tk.NSEW)#向四个方向拉伸填满MWindow帧
        
        self.pane1=tk.Frame(self.panewin)#bg="yellow")#添加水平方向的推拉窗组件
        self.pane2=tk.Frame(self.panewin,bg='red')#添加水平方向的推拉窗组件
        self.pane1.grid(row=0,column=0,sticky=tk.NSEW)
        self.pane2.grid(row=0,column=1,sticky=tk.NSEW)
        
        self.paneData=tk.Frame(self)#bg="yellow")#添加水平方向的推拉窗组件
        self.paneData.grid(row=1,column=0,sticky=tk.NSEW)
        
        sb = tk.Scrollbar(self.paneData)
        self.list=tk.Listbox(self.paneData,yscrollcommand= sb.set)
        sb.config(command=self.list.yview)
        self.list.pack(side='left',expand='yes',fill='both')
        sb.pack(side='right',expand='no',fill='y')
        self.initDateFrame()
        self.initCanvas()
        
    def initCanvas(self):
        self.canvas=tk.Canvas(self.pane1,bg='white')
        self.canvas.pack(side='left',expand='yes',fill='both')
        
        
    
    def initDateFrame(self):

        self.queryButton = tk.Button(self.pane2, text='查询', command=self.Query)
        self.queryButton.grid(row=0,column=0,sticky=tk.NE)
        
    def onYearChange(self):
        pass#print("ok")
        
    def onYear2Change(self):
        pass#print("hh")
        
    def Query(self):
        earn.CapScreen("")
        pilImage = Image.open("01.bmp")
        cropedIm = pilImage.resize((300, 400))
        self.tkImage = ImageTk.PhotoImage(image=cropedIm)
        self.canvas.create_image(300,0,anchor=tk.NE,image=self.tkImage)

# 创建一个Application对象app
root = tk.Tk()
# 设置窗口标题为'First Tkinter'
root.title ( '多米岛数据中心')
root.geometry("800x600")
app = Application(root)
# 主循环开始
root.mainloop()