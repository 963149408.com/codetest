import  os
import time
import imgOperate as imgOpt

bDebug=False
stopRead=None

def RunCmd(cmd):
    global bDebug
    ret=os.popen(cmd)
    retStr=ret.read()
    if bDebug:
        print(retStr)
    return retStr
def StartAdb():
    RunCmd("adb devices")
def CapScreen(savePath):
    RunCmd("adb shell screencap -p /sdcard/01.bmp")
    RunCmd("adb pull /sdcard/01.bmp ./")
def GoHome():
    KeyPress(3)
def GoBack():
    KeyPress(4)
def KeyPress(keyCode):
    RunCmd("adb shell input keyevent "+str(keyCode))
def Touch(x,y):
    RunCmd("adb shell input tap %d %d"%(x,y))
def initAll():
    global stopRead
    stopRead=imgOpt.initTarget("stopRead.bmp")
def TestScan():
    
    x,y=imgOpt.FindTarget("01.bmp",stopRead)
    if x!=-1:
        print(x,y)
        Touch(x,y)
'''
initAll()

i=50
while i>0:
    CapScreen("")
    x,y=imgOpt.FindTarget("01.bmp",stopRead)
    if x!=-1:
        time.sleep(1)
        continue
    time.sleep(20)
    GoBack()
    i-=1
print("complete")'''
