const path = require('path');  // node自带包
module.exports = {
  entry:'./tscript/main.ts',   // 打包对入口文件，期望打包对文件入口
  output:{
    filename:'[name].js',   // 输出文件名称
    path:path.resolve(__dirname,'../js')  //获取输出路径
  },
  mode: 'production',//'development',   // 整个mode 可以不要，模式是生产坏境就是压缩好对，这里配置开发坏境方便看生成对代码
  devtool: 'source-map',
  module:{
    rules: [{
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude:/node_modules/
        //exclude: /node_modules/
      }]
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"]      // 解析对文件格式
  },
}
