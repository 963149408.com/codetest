import * as THREE from 'three';
import * as ddd from "./test"
import Ammo from "./ammo.js"

// init
class Scene {
    m_scene: THREE.Scene;
    m_mesh: THREE.Mesh;
    constructor() {
        
        this.m_scene = new THREE.Scene();
        var transformAux1 = new Ammo.btTransform();
        
        const geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
        const material = new THREE.MeshNormalMaterial();
        this.m_mesh = new THREE.Mesh( geometry, material );
    }
    initScene() {
        
        this.m_scene.add( this.m_mesh );
    }

    getScene() {
        return this.m_scene;
    }

    frame(time:number) {
        this.m_mesh.rotation.x = time / 2000;
        this.m_mesh.rotation.y = time / 1000;
    }
}

const mainScene = new Scene()
mainScene.initScene()
ddd.Ojbk()

const camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.01, 10 );
camera.position.z = 1;

const renderer = new THREE.WebGLRenderer( { antialias: true } );
renderer.setSize( window.innerWidth, window.innerHeight );
renderer.setAnimationLoop( (time:number)=>{
    mainScene.frame(time)
    renderer.render( mainScene.getScene(), camera );
} );
document.body.appendChild( renderer.domElement );

window.addEventListener( 'resize', ()=>{
    let windowHalfX = window.innerWidth / 2;
    let windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );
} );
window.addEventListener('keydown',(ev)=>{
    console.log("key press:", ev.key);
})